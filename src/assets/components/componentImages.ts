/* eslint-disable */
const images = {
  lineChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/line-simple.webp?_v_=1612615474746',
  barChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/bar-background.webp?_v_=1612615474746',
  pieChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/pie-doughnut.webp?_v_=1612615474746',
  radarChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/radar.webp?_v_=1612615474746',
  treeChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/tree-basic.webp?_v_=1612615474746',
  kChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/custom-ohlc.webp?_v_=1612615474746',
  gaugeChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/gauge-simple.webp?_v_=1612615474746',
  graphChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/graph-simple.webp?_v_=1612615474746',
  funnelChart: 'https://cdn.jsdelivr.net/gh/apache/echarts-website@asf-site/examples/data/thumb/funnel.webp?_v_=1612615474746'
};

export default images;
