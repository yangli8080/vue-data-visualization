export interface Position {
  left: number;
  top: number;
}

export interface Size {
  width: number;
  height: number;
}
